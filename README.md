# Cloud and Edge computing assignment

## DevOps of a flask blog

- Create a multicontainer application
- .env to store environment variables
- refactored app.py so that database uri is built using env variables
- built a pipeline with gitlab ci/cd, the pipeline is composed of two stages, test and build
- test use flake8 for linting and check if new markdown files are pushed, if true check the validity of the new files
- build stage build the docker image and push it to the registry