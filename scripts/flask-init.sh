#!/bin/sh

# setup variabile FLASK_APP
export FLASK_APP=app.py

# inizializzo il database
flask db init

# migrazione del database
flask db migrate

# applico i cambiamenti
flask db upgrade

# permette l'esecuzione dei successivi parametri che gli verranno passati
exec "$@"