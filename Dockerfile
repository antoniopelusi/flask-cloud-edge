FROM python:3.11

# setup variabili d'ambiente di python
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# cambio directory
WORKDIR /app

# installo postgresql
COPY scripts/install-postgresql.sh scripts/install-postgresql.sh
RUN scripts/install-postgresql.sh

# installo requirements
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

# copio directory
COPY . .

# eseguo lo script che inizializza flask e il suo database
RUN scripts/flask-init.sh