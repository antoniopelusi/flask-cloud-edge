# controllo validità dell'header dei file markdown
import sys
import re

regex = [
    "title:\s.*",
    "subtitle:\s.*",
    "author:\s.*",
    "author_image:(\s.*\.(jpg|png|jpeg))",
    "date:\s+(\w+)\s+(\d+),\s+(\d+)",
    "image:(\s.*\.(jpg|png|jpeg))",
    "permalink:\s.*",
    "tags:\s+((\w+)(,\s\w+)*)",
    "shortcontent:\s.*"]

template = re.compile(regex.read())

for f in sys.argv[1:]:
    with open(f, mode="r") as current_file:
        if template.match(current_file.read()):
            print(f"File found: ", f)
        else:
            print(f"File not found: ", f)
            sys.exit(1)

sys.exit(0)